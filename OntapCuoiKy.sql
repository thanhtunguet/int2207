-- 1
SELECT * FROM `employees` WHERE `gender` = 'F';

-- 2
SELECT `emp_no`, CONCAT(`first_name`, ' ', `last_name`) as `fullname`, `birth_date`, `gender` FROM `employees`;

-- 3
SELECT * FROM `employees` WHERE `gender` = 'M' AND `hire_date` BETWEEN '1993-10-01' AND '1993-10-31';

-- 4
SELECT * FROM `salaries` WHERE `emp_no` = '100000';

-- 5
SELECT * FROM `employees` e RIGHT JOIN `titles` t ON t.`emp_no` = e.`emp_no` WHERE `title` = 'Engineer';

-- 6
